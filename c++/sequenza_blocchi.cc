#include <iostream>
#include <fstream>

using namespace std;

const char nome_file[] = "sequenza_blocchi.txt";

enum stato{occupato, libero};

struct sequenza{
	stato* blocchi;
    int num_blocchi;
};

void inizializza(sequenza &s){
    s.num_blocchi = 0;
}

void inizializza(sequenza &s, const int N){
    if(s.num_blocchi > 0){
        delete[] s.blocchi;
    }

    s.blocchi = new stato[N];
    for(int i = 0; i < N; i++){
        s.blocchi[i] = libero;
    }
    s.num_blocchi = N;
}

bool alloca_blocchi(sequenza &s, const int indice, const int n){
    // cout << "numero blocchi: " << s.num_blocchi << endl;
    if(indice+n > s.num_blocchi) {
        cout << "numero troppo grande" << endl;
        return false;
    }
    
    for(int i = indice; i < n; i++){
        if(s.blocchi[i] == occupato){
            cout << " esistono blocchi occupati" << endl;
            return false;
        }
    }
    
    for(int i = indice; i < indice+n; i++){
        cout << "indice:" << indice << endl;
        s.blocchi[i] = occupato;
    }
    return true;
}

void stampa_stato(sequenza &s){
    for(int i = 0; i < s.num_blocchi; i++){
        if(s.blocchi[i] == occupato){
            cout << '*';
        }
        else if(s.blocchi[i] == libero){
            cout << '-';
        }
    }
}

bool salva_stato(sequenza &s){
    ofstream of(nome_file);
    if(!of) return false;

    of << s.num_blocchi << endl;
    for(int i = 0; i < s.num_blocchi; i++){
        if(s.blocchi[i] == occupato){
            of << '*';
        }
        else{
            of << '-';
        }
        of.flush();
    }
    of.close();
    return true;
}

bool carica_stato(sequenza &s){
    ifstream inf(nome_file);
    if(!inf) return false;
    
    int N;
    inf >> N;
    inizializza(s, N);

    char stato;
    for(int i = 0; i < s.num_blocchi; i++){
        inf >> stato;
        if(stato == '*'){
            s.blocchi[i] = occupato;
        }
        else if (stato == '-'){
            s.blocchi[i] = libero;
        }
        else{
            return false;
        }
    }
    inf.close();
    return true;
}

int main()
{
    sequenza s;
    inizializza(s);

	const char menu[] =
		"1. Inizializza sequenza\n"
		"2. Alloca blocchi\n"
		"3. Stampa stato\n"
		"4. Salva stato\n"
		"5. Carica stato\n"
		"6. Stampa blocchi liberi da indice\n"
		"7. Cerca ed alloca blocchi\n"
		"8. Cerca ed alloca blocchi 2\n"
		"9. Esci\n";

	while (true) {
		cout<<menu<<endl;

		int scelta;
		cin>>scelta;

		switch (scelta) {
		case 1:
        {
            int N;
            cout << "inserisci il N: ";
            cin >> N;
            inizializza(s, N);
			break;
        }
		case 2:{
            int indice = 0;
            int n = 0;
            cout << "inserisci indice: ";
            cin >> indice;
            cout << "inserisci il n: ";
            cin >> n;
            cout << "indice:" << indice << " n = " << n << endl;
            alloca_blocchi(s, indice, n);
            break;
        }
			
		case 3:{
            stampa_stato(s);
            break;
        }
			
		case 4:{
            salva_stato(s);
            break;

        }
			
		case 5:{
            carica_stato(s);
            break;
        }
			
		case 6:
			break;
		case 7:
			break;
		case 8:
			break;
		case 9:
			return 0;
		default:
			cout<<"Scelta errata"<<endl;
		}
	}
}
