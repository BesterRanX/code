#include <iostream>

using namespace std ;

enum tipo_quadrilatero{quadrato, rettangolo, trapezio};

struct quadrilatero{
	tipo_quadrilatero tipo;
	int lato1, lato2, lato3, lato4;
};

struct sequenza{
	quadrilatero* sequenza;
    int num_seq;
	int max_seq;
};


void inizializza(sequenza &s){
    s.max_seq = s.num_seq = 0;
}

void modifica_lunghezza_massima(sequenza &s, const int N){
    if(s.max_seq > 0){
        delete[] s.sequenza;
    }
    s.sequenza = new quadrilatero[N];
    s.num_seq = 0;
    s.max_seq = N;
}

bool inserisci_quadrilatero(sequenza &s, const quadrilatero q){
    if(s.num_seq == s.max_seq) return false;
    
    s.num_seq++;
    s.sequenza[s.num_seq] = q;
}

void stampa_sequenza(sequenza &s){
    
}



int main()
{
    sequenza s;
    inizializza(s);
	const char menu[] =
		"1. Modifica lunghezza massima\n"
		"2. Inserisci quadrilatero\n"
		"3. Stampa sequenza\n"
		"4. Salva stato\n"
		"5. Carica stato\n"
		"6. Controlla quadrilatero\n"
		"7. Esci\n";

	while (true) {
		cout<<menu<<endl ;

		int scelta ;
		cin>>scelta ;

		switch (scelta) {
		case 1:
			break ;
		case 2:
			break ;
		case 3:
			break ;
		case 4:
			break ;
		case 5:
			break ;
		case 6:
			break ;
		case 7:
			return 0 ;
		default:
			cout<<"Scelta sbagliata"<<endl ;

		}
	}

	return 1 ;
}
