#include <iostream>
#include <fstream>

using namespace std;

const int N = 20;
const char f_insieme[] = "insieme_codici.txt";

struct codice_t{
   int valore;
   int occorrenze;
};

struct insieme_t{
   codice_t* codici;
   int max_cod;
   int num_cod = 0;
};

void inizializza(insieme_t &ins, int N){
   if(ins.num_cod > 0)
       delete[] ins.codici;
   ins.codici = new codice_t[N];
   ins.max_cod = N;
   ins.num_cod = 0;
}
insieme_t crea_insieme(const int N){
   insieme_t ins;
   inizializza(ins, N);
   return ins;
}

bool esiste_codice(insieme_t &ins, const codice_t &c){
   for(int i = 0; i < ins.num_cod; i++){
      if(c.valore == ins.codici[i].valore){
         ins.codici[i].occorrenze++;
         return true;
      }
   }
   return false;
}

bool aggiungi_codice(insieme_t &ins, const codice_t &c){
   if(ins.num_cod >= N)
      return false;
   if(!esiste_codice(ins, c)){
      ins.codici[ins.num_cod] = c;
      ++ins.num_cod;
   }
   // cout << "codice aggiunto " << ins.codici[ins.num_cod].valore;
   return true;
}

void ostream_insieme(insieme_t &ins, ostream &os, bool is_file){
   if(is_file)
      os << ins.max_cod << endl << ins.num_cod << endl;
   for(int i = 0; i < ins.num_cod; i++){
      os << ins.codici[i].valore;
      if(!is_file) 
           os << ":"; 
      os << " " << ins.codici[i].occorrenze << endl;
   }
}

void stampa_insieme(insieme_t &ins){
    cout << "*** STAMPA ***" << endl;
    ostream_insieme(ins, cout, false);
}

bool salva_insieme(insieme_t &ins){
    ofstream ofs(f_insieme);
    if(!ofs) return false;
    ostream_insieme(ins, ofs, true);
    ofs.close();
    return true;
}

bool carica_insieme(insieme_t &ins){
    ifstream ifs(f_insieme);
    if(!ifs) return false;
    
    // inizializza insieme
    int max_cod;
    ifs >> max_cod;
    inizializza(ins, max_cod);
    ifs >> ins.num_cod;
    
    // carica i contenuti
    for(int i = 0; i < ins.num_cod; i++)
       ifs >> ins.codici[i].valore >> ins.codici[i].occorrenze;
    return true;
}

void stampa_intersezione(insieme_t &ins1, insieme_t &ins2){
    // intersezione con priorita' il minore.
    for(int i = 0; i < ins1.num_cod; i++)
       for(int j = 0; j < ins2.num_cod; j++)
          if(ins1.codici[i].valore == ins2.codici[j].valore)
             if(ins1.codici[i].occorrenze <= ins2.codici[j].occorrenze)
                cout << ins1.codici[i].valore << ": " << ins1.codici[i].occorrenze << endl; 
             else
                cout << ins2.codici[i].valore << ": " << ins2.codici[j].occorrenze << endl;
}

int main()
{
        insieme_t ins;
	const char menu[] =
		"1. Crea insieme\n"
		"2. Aggiungi codice\n"
		"3. Stampa insieme\n"
		"4. Salva insieme\n"
		"5. Carica insieme\n"
		"6. Stampa intersezione\n"
		"7. Esci\n";

	while (true) {
		cout<<menu<<endl;

		int scelta;
		cin>>scelta;

		switch (scelta) {
		case 1: ins = crea_insieme(N);
			break;
		case 2:{
		    codice_t c;
		    cout << "inserisci valore: ";
		    cin >> c.valore;
		    c.occorrenze = 1;
		    aggiungi_codice(ins, c);
		    break;
	        }
		case 3:stampa_insieme(ins);
			break;
		case 4: salva_insieme(ins);
			break;
		case 5: carica_insieme(ins);
			break;
		case 6: {
		        insieme_t ins2 = crea_insieme(4);
			aggiungi_codice(ins2, {10, 1});
			for (int i = 0 ; i < 3; i++) {
			    aggiungi_codice(ins2, {95, 1});
			    aggiungi_codice(ins2, {3214, 1});
			}
			aggiungi_codice(ins2, {814, 1});
			cout<<"Insieme 2:"<<endl;
			ostream_insieme(ins2, cout, false);
			cout<<endl
			    <<"Intersezione: "<<endl;
			stampa_intersezione(ins, ins2);
			break;
	        }
		case 7:
			return 0;
		default:
			cout<<"Scelta sbagliata"<<endl;
		}
	}

	return 1;

}
