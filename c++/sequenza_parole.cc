#include <iostream>
#include <cstring>
#include <fstream>

using namespace std;

const char nome_file[] = "sequenza.txt";

const int N = 10; // lunghezza sequenza
const int L = 20; // lunghezza parola

struct sequenza{
	char* sequenza_parole;
    int* indici_parole; 
	int lun_seq; // lunghezza sequenza
    int num_parole;
};

void inizializza(sequenza &s){
	s.lun_seq = s.num_parole = 0;
}

bool inserisci_parola(struct sequenza &s, const char* parola){
	int lun_parola = strlen(parola);
    int vecchio_lun_parola = s.num_parole;

    // cout << "lunghezza della parola inserita:" << lun_parola << endl;
    if((s.lun_seq+lun_parola) > N*L){
        return false;
    }

    int lun_totale = s.lun_seq + lun_parola;
    char* nuova_sequenza = new char[lun_totale]; // 1 di riserva per \0
    
    //copia la vecchia sequenza
    for(int i = 0; i < s.lun_seq; i++){
        nuova_sequenza[i] = s.sequenza_parole[i];
    }

    //aggiungi nuova parola
    for(int i=0; i<lun_parola; i++){
        nuova_sequenza[s.lun_seq+i] = parola[i];
    }

    s.lun_seq = lun_totale;
    // cout << "lunghezza totale: " << s.lun_seq;

    /*/stampa
    for(int i = 0; i < lun_totale; i++){
        cout << nuova_sequenza[i];
    }
    cout << endl;
    */

    int* nuovo_indice = new int[s.num_parole+1];

    //copia il vecchio indice
    for(int i = 0; i < s.num_parole; i++){
        nuovo_indice[i] = s.indici_parole[i];
    }

    //aggiungi il nuovo indice
    nuovo_indice[s.num_parole] = s.lun_seq - 1;
    s.num_parole++;
    // cout << "nuovo indice: " << nuovo_indice[1];

    if(vecchio_lun_parola > 0){
        delete[] s.indici_parole;
        delete[] s.sequenza_parole;
    }
    delete[] parola;
    
    s.sequenza_parole = nuova_sequenza;
    s.indici_parole = nuovo_indice;
    /*
    for(int i = 0; i < s.lun_seq; i++) {
        cout << s.sequenza_parole[i];
    }
    cout << "fine funzione" << " numero parola " << s.num_parole << " sequenza lunghezza " << s.lun_seq << endl;
    */
    //assegna nuova sequenza
    return true;
}

void stampa_sequenza(struct sequenza &s){
    // cout << "funzione richiamata" << endl << "lunghezza sequenza: " << s.lun_seq << endl;
    for(int p = 0, i = 0; p < s.lun_seq; p++){
        cout << s.sequenza_parole[p];
        if(p == s.indici_parole[i]){
            cout << endl;
            i++;
        }
    }
    cout << endl;
}

void sposta_in_prossima(struct sequenza &s, int idx, int n){
    int resto = 0;
    int vecchio_indice = 0;

    for(int i = idx; i < s.num_parole; i++){
        unsigned int lunghezza_parola = s.indici_parole[i] - vecchio_indice;
        if(lunghezza_parola > n){
            resto = lunghezza_parola - n ;
        }
        vecchio_indice = s.indici_parole[i];
        s.indici_parole[i] -= resto;
    }
}

bool salva_sequenza(struct sequenza &s){
    ofstream of(nome_file);
    for(int i = 0, p = 0; i < s.lun_seq; i++){
        of << s.sequenza_parole[i];
        if(i == s.indici_parole[p]){
            of << endl;
            p++;
        }
    }
    of.flush();
    of.close();
    return of.good();
}

int main()
{
    sequenza s;
	inizializza(s);
	const char menu[] =
		"1. Inserisci parola\n"
		"2. Stampa sequenza\n"
		"3. Salva sequenza\n"
		"4. Carica sequenza\n"
		"5. Sostituisci caratteri da prossima parola\n"
		"6. Esci\n";


	while (true) {
		cout<<menu<<endl;

		int scelta;
		cin>>scelta;

		switch (scelta) {
		case 1: {
            char* parola = new char[L];
            cout << "inserisci parola: ";
            cin >> parola;
            inserisci_parola(s, parola);
            break;
        }
		case 2:{
            stampa_sequenza(s);
            break;
        }
			
		case 3:salva_sequenza(s);
			break;
		case 4:
			break;
		case 5:{
            int idx = 0, n = 0;
            cout << "inserisci idx: ";
            cin >> idx;
            cout << "inserisci n: ";
            cin >> n;
            sposta_in_prossima(s, idx, n);
            break;
        }
			
		case 6:
			return 0;
		default:
			cout<<"Scelta errata"<<endl;
		}
	}


	return 1;
}
