#include <iostream>
#include <fstream>
#include <cstring>


using namespace std;
const char file_sequenza[] = "sequenza_comando.txt";
const int MAX = 20;
const int N = 15;

enum type_comando{controllo = 5, lettura = 10};

struct comando{
   type_comando t_comando;
   int codice;
   char* buffer;
};

struct sequenza_comando{
   comando* sequenza;
   int num_seq = 0;
   int prossimo = 0;
};

void inizializza(sequenza_comando &s){
   if(s.num_seq > 0){
      if(s.prossimo > 0){
          for(int i = 0; i < s.prossimo; i++)
              if(s.sequenza[i].t_comando == lettura) 
                  delete[] s.sequenza[i].buffer;
      }
      delete[] s.sequenza;
   }
   s.sequenza = new comando[MAX];
   s.num_seq = s.prossimo = 0;
}

bool inserisci_comando(sequenza_comando &s, const type_comando c){
   if(s.num_seq >= MAX){
       cout << "raggiunto numero massimo di comandi, impossibile inserire" << endl;
       return false; 
   }

   //inserisci il comando
   s.sequenza[s.num_seq].t_comando = c;
   if(c == controllo){
       cout << "inserisci il codice di controllo:";
       int codice = 0;
       cin >> s.sequenza[s.num_seq].codice;
   }
   else{
       s.sequenza[s.num_seq].buffer = 0;
   }
   s.num_seq++;
   return true;
}

bool esegui_prossimo_comando(sequenza_comando &s){
   if(s.prossimo == s.num_seq) return false;

   if(s.sequenza[s.prossimo].t_comando == lettura){
         // cout << "lettura";
         s.sequenza[s.prossimo].buffer = new char[N];

         cout << "inserisci la parola:";
         cin >> s.sequenza[s.prossimo].buffer;
         // cout << "parola inserita: " << s.sequenza[s.prossimo].buffer << endl;
    }
    s.prossimo++;
    return true;
}

bool output_stato(sequenza_comando &s, ostream &os, bool is_file){
   if(is_file){
      os << s.num_seq << endl;
      os << s.prossimo << endl;
   }

   int i;
 
   if(!is_file) os << "*** ESEGUITI ***" << endl;
   for(i = 0; i < s.prossimo; i++){
       if(s.sequenza[i].t_comando == controllo)
          os << "controllo " << s.sequenza[i].codice << endl;
       else
          os << "lettura " << s.sequenza[i].buffer << endl;    
   }

   if(!is_file) os << "*** DA ESEGUIRE ***" << endl;
   for(;i < s.num_seq; i++){
       if(s.sequenza[i].t_comando == controllo)
          os << "controllo" << " " << s.sequenza[i].codice << endl;
       else
          os << "lettura" << endl;    
   }
   return true;
}


void stampa_comandi(sequenza_comando &s){
   output_stato(s, cout, false);
}

bool salva_stato(sequenza_comando &s){
   ofstream ofs(file_sequenza);
   output_stato(s, ofs, true);
   return true;
}

bool carica_stato(sequenza_comando &s){
   // cout << "inizializzata";
   ifstream ifs(file_sequenza);
   
   if(!ifs) return false;

   inizializza(s);
   
   ifs >> s.num_seq >> s.prossimo;
   
   for(int i = 0; i < s.num_seq; i++){
      char string_tmp[N];
      ifs >> string_tmp;
      // cout << string_tmp;
      if(strcmp("controllo",string_tmp) == 0)
         s.sequenza[i].t_comando = controllo;
      else
         s.sequenza[i].t_comando = lettura;

      if(s.sequenza[i].t_comando == controllo)
         ifs >> s.sequenza[i].codice;
      else if(i < s.prossimo){
              s.sequenza[i].buffer = new char[N];
              ifs >> s.sequenza[i].buffer;
      }
   }
   return true;
}

void vai_su_prossimo_buffer(const sequenza_comando &s, int &idx_com){
    if (idx_com == s.prossimo)
        return;

    while (idx_com < s.prossimo && s.sequenza[idx_com].t_comando != lettura)
	idx_com++;
}

void vai_su_prossima_posizione(const sequenza_comando &s, int &idx_com, int &idx_buf){
     if (s.sequenza[idx_com].buffer[idx_buf+1] != '\0') {
	 idx_buf++;
	 return;
     }
     idx_com++; // parti dal prossimo indice e cerca la prossima lettura.
     vai_su_prossimo_buffer(s, idx_com);
     idx_buf = 0;
}

bool cerca_parola(const sequenza_comando &s, const char parola[])
{ 
      int idx_com = 0, idx_buf = 0;
      vai_su_prossimo_buffer(s, idx_com);
      while (idx_com < s.prossimo) {
	   if (parola[0] == s.sequenza[idx_com].buffer[idx_buf]) {
	       int p = 0;
	       int idx_com2 = idx_com, idx_buf2 = idx_buf;
               //finche i caratteri coincidono aumenta p
	       do {
		    p++;
		    vai_su_prossima_posizione(s, idx_com2, idx_buf2);
	       } while(parola[p] != '\0' && idx_com2 < s.prossimo &&
		    s.sequenza[idx_com2].buffer[idx_buf2] == parola[p]);

       // se infine l'ultimo carattere della parola e '\0', tutto e andato a buon fine
	       if (parola[p] == '\0')
		    return true;
	    }
	    vai_su_prossima_posizione(s, idx_com, idx_buf);
      }
      return false;
}


int main()
{
        sequenza_comando s;
        inizializza(s);
	const char menu[] =
		"1. Inserisci comando\n"
		"2. Esegui prossimo comando\n"
		"3. Stampa comandi\n"
		"4. Salva stato\n"
		"5. Carica stato\n"
		"6. Cerca parola\n"
		"7. Esci\n";

	while (true) {
		cout<<menu<<endl;
		int scelta;
		cin>>scelta;

		switch (scelta) {
		case 1: {
                    char c;
                    cout << "inserisci il tipo di comando[c/l]:";
                    cin >> c;
                    if(c == 'c') inserisci_comando(s, controllo);
                    else if(c == 'l') inserisci_comando(s, lettura);

                    break;
                }
		case 2:{
                    esegui_prossimo_comando(s);
	            break;
                }
		case 3: stampa_comandi(s);
			break;
		case 4:salva_stato(s);
			break;
		case 5: carica_stato(s);
			break;
		case 6: {
                   char parola[20];
                   cout << "inserisci parola: ";
                   cin >> parola;
                   cout << "parola inserita";
                   bool trovato = cerca_parola(s, parola); 
                   if(trovato) cout << "parola trovata";
	           break;
                }
		case 7: 
			return 0;
		default:
			cout<<"Scelta errata"<<endl;
		}
	}
	return 1;
}
